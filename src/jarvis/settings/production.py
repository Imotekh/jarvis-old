"""
Django settings for jarvis project.

Generated by 'django-admin startproject' using Django 2.2.3.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""
from .base import *

import os

from dotenv import load_dotenv

load_dotenv(verbose=True, override=True, dotenv_path='.env')

SECRET_KEY = os.environ.get('TOKEN_PROD')
BOT_TOKEN = os.environ.get('BOT_TOKEN')

DEBUG = False

ALLOWED_HOSTS = ['jarvis.imotekh.fr']

ROOT_URLCONF = 'jarvis.urls'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.postgresql_psycopg2',
        'NAME':     os.environ.get('DB_NAME'),
        'USER':     os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST':     os.environ.get('DB_HOST'),
        'PORT':     os.environ.get('DB_PORT'),
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/www/jarvis/static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = '/www/jarvis/media/'
