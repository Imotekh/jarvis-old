from django.contrib import admin

from jarvis_junk.models import Reply, W40kQuote, Image, ImageCmd, ImageCmdAlias

@admin.register(Reply)
class AdminReply(admin.ModelAdmin):

    list_display = ('message',)
    list_filter = ('message',)
    ordering = ('message',)
    search_fields = ('message',)
    fields = ('message',)

@admin.register(W40kQuote)
class AdminW40kQuote(admin.ModelAdmin):

    list_display = ('quote',)
    list_filter = ('quote',)
    ordering = ('quote',)
    search_fields = ('quote',)
    fields = ('quote',)

@admin.register(Image)
class AdminImage(admin.ModelAdmin):

    list_display = ('image',)
    list_filter = ('image',)
    ordering = ('image',)
    search_fields = ('image',)
    fields = ('image',)

@admin.register(ImageCmd)
class AdminImageCmd(admin.ModelAdmin):

    list_display = ('name',)
    list_filter = ('name',)
    ordering = ('name',)
    search_fields = ('name',)
    fields = ('name', 'pool')

@admin.register(ImageCmdAlias)
class AdminImageCmdAlias(admin.ModelAdmin):

    list_display = ('alias', 'cmd')
    list_filter = ('alias', 'cmd')
    ordering = ('alias', 'cmd')
    search_fields = ('alias', 'cmd')
    fields = ('alias', 'cmd')
