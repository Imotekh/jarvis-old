from django.db import models

from jarvis_discord.models import Member

class Reply(models.Model):

    message = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.message

class W40kQuote(models.Model):

    quote = models.TextField(max_length=500, unique=True)

    def __str__(self):
        return self.quote

class Image(models.Model):

    image = models.ImageField(upload_to='images')

    def __str__(self):
        return self.image.name

class ImageCmd(models.Model):

    name = models.CharField(max_length=100, unique=True)
    pool = models.ManyToManyField(Image)

    def __str__(self):
        return self.name

class ImageCmdAlias(models.Model):

    alias = models.CharField(max_length=100, unique=True)
    cmd = models.ForeignKey(ImageCmd, on_delete=models.CASCADE)

    def __str__(self):
        return self.alias
