from django.contrib.auth.models import User
from django.db import models

class Member(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    discord = models.CharField(default='', max_length=20, unique=True)
    available = models.BooleanField(default=True)
    
    def __str__(self):
        return self.user.username
