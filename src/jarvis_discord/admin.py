from django.contrib import admin

from jarvis_discord.models import Member

@admin.register(Member)
class AdminMember(admin.ModelAdmin):

    list_display = ('user', 'discord', 'available')
    list_filter = ('user',)
    ordering = ('user',)
    search_fields = ('user',)
    fields = ('user', 'discord', 'available')
