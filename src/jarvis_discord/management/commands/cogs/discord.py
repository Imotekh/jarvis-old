import logging

import discord

from discord.ext import commands

logger = logging.getLogger('discord')

class DiscordCog(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='stop', description='Stop the bot')
    async def stop(self, ctx):
        if ctx.author.id == 298090489507217419:
            await self.bot.close()
