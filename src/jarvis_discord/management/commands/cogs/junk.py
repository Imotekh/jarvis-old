import logging
import random
import requests
import json

from asgiref.sync import sync_to_async

from os import listdir
from os.path import isfile, join

from django.conf import settings

import discord

from bs4 import BeautifulSoup

from discord.ext import commands, tasks

from jarvis_junk.models import W40kQuote, Reply, ImageCmd, ImageCmdAlias

logger = logging.getLogger('discord')

def randchar(c):
    """ Replace randomly a char to cancerize a string """

    if random.random() > 0.5:
        c = c.upper()
        if c == 'E' and random.random() < 0.25:
            return '3'
        if c == 'O' and random.random() < 0.4:
            return '0'
        if c == 'S' and random.random() < 0.2:
            return '$'
        return c
    return c.lower()

def get_slogan(word):
    """ Get a random slogan """

    req = requests.get(f"https://oberlo-growth-tools.shopifycloud.com/slogan/search?utf8=%E2%9C%93&term={word}&button=")
    soup = BeautifulSoup(req.text, 'html.parser')
    slogan = [mantra.text for mantra in soup.find_all("span", "business-name")][0]
    logger.info("Retrieve slogan : {}".format(slogan))
    return slogan

def get_cat():
    """ Get a random cat from API """

    cat_url = requests.get("https://api.thecatapi.com/v1/images/search").json()[0]['url']
    logger.info("Retrieve cat : {}".format(cat_url))
    return cat_url

def get_random_file(directory):
    onlyfiles = [f for f in listdir(directory) if isfile(join(directory, f))]
    return discord.File(directory + random.choice(onlyfiles))

def get_random_reply():
    return Reply.objects.order_by('?').first()

def get_random_quote():
    return W40kQuote.objects.order_by('?').first()

def get_image_from_pool(pool):
    image_url = None

    cmd = ImageCmd.objects.filter(name=pool.lower()).first()
    if not cmd:
        alias = ImageCmdAlias.objects.filter(alias=pool.lower()).first()
        if alias:
            cmd = alias.cmd
    if cmd:
        image_url = cmd.pool.order_by('?').first().image.url
        image_url = "https://{}{}".format(settings.ALLOWED_HOSTS[0], image_url)
    return image_url

def get_img_cmd_help():
    help_str = '```\n'
    help_str += "List of all image pool associated with aliases :\n"

    for cmd in ImageCmd.objects.all():
        aliases = cmd.imagecmdalias_set.all()
        aliases_list = []
        for alias in aliases:
            aliases_list.append(alias.alias)
        if aliases_list:
            help_str += "- {} {}\n".format(cmd.name, aliases_list)
        else:
            help_str += "- {}\n".format(cmd.name)
    help_str += '```'
    return help_str

class JunkCog(commands.Cog):
    """ A cog to regroup all dumb things i can implement """

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    SALUTATIONS = ['bonjour', 'bjr', 'slt', 'yo', 'cc', 'coucou', 'hello']
    PENIS = ['bite', 'pénis', 'penis', 'chibre', 'poutre', 'sexe']
    IMPERIUM = ["empereur", "imperium", "prier", "prions", "prie"]
    QUOI_FEUR = ["quoi", "coi", "koa", "koi"]

    @commands.Cog.listener()
    async def on_message(self, message):
        """ Check all messages on visible channels to react with emojis or other dumb things """

        content = message.content.lower().replace('?', '').replace('.', '').replace(',', '')
        word_array = content.split(' ')

        if not message.author.bot and not content.startswith(">slogan"):
            reply = await sync_to_async(get_random_reply)()
            for user in message.mentions:
                if reply and user.id == 676529415273840691:
                    await message.channel.send(reply.message)

            if "jarvis" in word_array:
                await message.channel.send(get_slogan("Jarvis"))
            if "rip" in word_array:
                await message.add_reaction('🪦')

        if any(txt in word_array for txt in self.SALUTATIONS):
            await message.add_reaction('👋')
        if any(txt in word_array for txt in self.PENIS):
            await message.add_reaction('🍆')
        if any(txt in word_array for txt in self.IMPERIUM):
            await message.channel.send(await sync_to_async(get_random_reply)())

        content = content.replace(' ', '')
        if any(content.endswith(suffix) for suffix in self.QUOI_FEUR):
            await message.add_reaction('🇫')
            await message.add_reaction('🇪')
            await message.add_reaction('🇺')
            await message.add_reaction('🇷')

    @commands.command(name='image', description='Get a random image from the sub pool', aliases=['images', 'i'])
    async def _i(self, ctx, subtype: str):
        if ctx.guild:
            await ctx.message.delete()
        if subtype == 'help':
            await ctx.send(await sync_to_async(get_img_cmd_help)())
        else:
            image_url = await sync_to_async(get_image_from_pool)(subtype)
            if image_url:
                await ctx.send(image_url)
            else:
                await ctx.send('Unknown subtype : "{}"'.format(subtype))

    @commands.command(name='slogan', description='Get a wonderful slogan')
    async def _slogan(self, ctx, word: str):
        if ctx.guild:
            await ctx.message.delete()
        await ctx.send(get_slogan(word))

    @commands.command(name='w40k', description='Get a random w40k quote')
    async def _w40k(self, ctx):
        if ctx.guild:
            await ctx.message.delete()
        await ctx.send(await sync_to_async(get_random_reply)())

    @commands.command(name='cancer')
    @commands.guild_only()
    async def _cancer(self, ctx, *, arg):
        """ Cancerize a string by replacing some chars and changing case randomly """

        if ctx.guild:
            await ctx.message.delete()
        await ctx.send(ctx.author.mention + ' : ' + ''.join(map(randchar, arg)))

    @commands.command(name='cat', description='Get a cute kitty', aliases=['kitty', 'potitchat', 'potichat', 'catroulette'])
    async def _cat(self, ctx):
        """ Send a random cat to the channel """

        if ctx.guild:
            await ctx.message.delete()
        await ctx.send(get_cat())
    
    @commands.command(name='goumage', aliases=['bagarre', 'goumer', 'bagarrer', 'goummer', 'goummage'])
    async def _goumage(self, ctx):
        """ Bagarre """

        if ctx.guild:
            await ctx.message.delete()
        await ctx.send(ctx.author.mention + " : (ง'̀-'́)ง")

    @commands.command(name='caner', aliases=[])
    async def _caner(self, ctx):
        """ Caner """

        if ctx.guild:
            await ctx.message.delete()
        await ctx.send(ctx.author.mention + " : Mes aievx, qvelle fort belle jovrnee povr ressentir en mien coevr vne si belle envie de caner")
