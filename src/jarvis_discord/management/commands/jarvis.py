# https://discord.com/oauth2/authorize?client_id=676529415273840691&permissions=2050227824&scope=bot

import random
import logging

from django.conf import settings
from django.core.management.base import BaseCommand

from discord.ext import commands

from .cogs.junk import JunkCog
from .cogs.discord import DiscordCog
from .cogs.help import HelpCog

logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='a')
handler.setFormatter(logging.Formatter('[%(asctime)s][%(levelname)s]: %(message)s', datefmt='%d/%m/%Y %H:%M:%S'))
logger.addHandler(handler)

bot = commands.Bot(command_prefix='>')

@bot.event
async def on_ready():
    logger.info(f'Logged in as {bot.user}')

@bot.event
async def on_command(ctx):
    if ctx.guild:
        logger.info(f"On server \"{ctx.guild.name}#{ctx.channel.name}\", \"{ctx.author.name}\" executed command : {ctx.message.content}")
    else:
        logger.info(f"On DM to \"{ctx.author.name}\" executed command : {ctx.message.content}")

class Command(BaseCommand):

    help = 'Start python bot'

    def handle(self, *args, **kwargs):
        bot.add_cog(JunkCog(bot))
        bot.add_cog(DiscordCog(bot))
        bot.add_cog(HelpCog(bot))
        bot.run(settings.BOT_TOKEN)
