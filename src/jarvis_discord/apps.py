from django.apps import AppConfig


class JarvisDiscordConfig(AppConfig):
    name = 'jarvis_discord'
